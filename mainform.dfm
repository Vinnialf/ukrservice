object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 441
  ClientWidth = 778
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Position = poDesigned
  TextHeight = 15
  object Label1: TLabel
    Left = 352
    Top = 8
    Width = 43
    Height = 15
    Caption = #1062#1110#1085#1072' '#1074#1110#1076
  end
  object Label2: TLabel
    Left = 352
    Top = 29
    Width = 13
    Height = 15
    Caption = #1076#1086
  end
  object DBGrid2: TDBGrid
    Left = -8
    Top = 72
    Width = 585
    Height = 161
    DataSource = DataSource4
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'TRADE_MARK'
        Title.Caption = #1053#1072#1079#1074#1072' '#1090#1086#1088#1075#1086#1074#1086#1111' '#1084#1072#1088#1082#1080
        Width = 131
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SERMENT'
        Title.Caption = #1053#1072#1079#1074#1072' '#1089#1077#1075#1084#1077#1085#1090#1091
        Width = 133
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PROD_ID'
        Title.Caption = #1053#1072#1079#1074#1072' '#1090#1086#1074#1072#1088#1091
        Width = 101
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRICE'
        Title.Caption = #1062#1110#1085#1072
        Width = 92
        Visible = True
      end>
  end
  object Button2: TButton
    Left = 480
    Top = 327
    Width = 75
    Height = 25
    Caption = #1047#1074#1110#1090' + pdf'
    TabOrder = 1
    OnClick = Button2Click
  end
  object button: TButton
    Left = 480
    Top = 296
    Width = 75
    Height = 25
    Caption = '2 ODT'
    TabOrder = 2
    OnClick = buttonClick
  end
  object CheckBox1: TCheckBox
    Left = 8
    Top = 49
    Width = 97
    Height = 17
    Caption = #1057#1086#1088#1090#1091#1074#1072#1090#1080
    TabOrder = 3
    OnClick = CheckBox1Click
  end
  object CheckBox2: TCheckBox
    Left = 111
    Top = 49
    Width = 97
    Height = 17
    Caption = #1057#1086#1088#1090#1091#1074#1072#1090#1080
    TabOrder = 4
    OnClick = CheckBox2Click
  end
  object Edit1: TEdit
    Left = 401
    Top = 0
    Width = 121
    Height = 23
    NumbersOnly = True
    TabOrder = 5
    Text = '0'
    OnChange = Edit1Change
  end
  object Edit2: TEdit
    Left = 401
    Top = 29
    Width = 121
    Height = 23
    TabOrder = 6
    Text = '99999'
    OnChange = Edit2Change
  end
  object DBGrid1: TDBGrid
    Left = 208
    Top = 260
    Width = 129
    Height = 120
    DataSource = DataSource1
    Options = [dgColumnResize, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleHotTrack]
    TabOrder = 7
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    Visible = False
    Columns = <
      item
        Expanded = False
        FieldName = 'TRADE_MARK'
        Width = 50
        Visible = True
      end>
  end
  object Button1: TButton
    Left = 208
    Top = 386
    Width = 75
    Height = 25
    Caption = #1079#1072#1089#1090#1086#1089#1091#1074#1072#1090#1080
    TabOrder = 8
    Visible = False
    WordWrap = True
    OnClick = Button1Click
  end
  object Button3: TButton
    Left = 208
    Top = 239
    Width = 75
    Height = 25
    Caption = #1060#1110#1083#1100#1090#1088#1091#1074#1072#1090#1080
    TabOrder = 9
    OnClick = Button3Click
  end
  object DataSource1: TDataSource
    DataSet = Trade_mark
    Left = 600
    Top = 32
  end
  object UkrserviceConnection: TFDConnection
    ConnectionName = 'ukrservice'
    Params.Strings = (
      'Database=C:\UKRSERVICE.GDB')
    Connected = True
    LoginPrompt = False
    Left = 522
    Top = 208
  end
  object DataSource2: TDataSource
    DataSet = ProductsReport
    Left = 736
    Top = 104
  end
  object ProductsReport: TFDQuery
    Active = True
    AutoCalcFields = False
    Indexes = <
      item
        Name = 'prod_id'
        Fields = 'PROD_ID'
      end
      item
        Name = 'price'
        Fields = 'PRICE'
      end>
    IndexFieldNames = 'FK'
    MasterSource = DataSource3
    MasterFields = 'KEYFIELD'
    Connection = UkrserviceConnection
    SQL.Strings = (
      
        'SELECT products.prod_id, products.price, products.serment, produ' +
        'cts.trade_mark, trim(products.trade_mark) || products.serment as' +
        ' fk FROM PRODUCTS  order by fk'
      '')
    Left = 713
    Top = 154
    object ProductsReportPROD_ID: TStringField
      FieldName = 'PROD_ID'
      Origin = 'PROD_ID'
      Required = True
      FixedChar = True
      Size = 9
    end
    object ProductsReportPRICE: TCurrencyField
      FieldName = 'PRICE'
      Origin = 'PRICE'
    end
    object ProductsReportFK: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'FK'
      Origin = 'FK'
      ProviderFlags = []
      ReadOnly = True
      Size = 13
    end
  end
  object frxReport1: TfrxReport
    Version = '2023.2.4'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 45171.772962858800000000
    ReportOptions.LastChange = 45172.784269189800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    Left = 688
    Top = 216
    Datasets = <
      item
        DataSet = frxDBDataset1
        DataSetName = 'frxDBDataset1'
      end
      item
        DataSet = frxDBDataset2
        DataSetName = 'frxDBDataset2'
      end
      item
        DataSet = frxDBDataset3
        DataSetName = 'frxDBDataset3'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677179340000000000
          Top = -0.000000230000000000
          Width = 268.346628830000000000
          Height = 18.897649770000000000
          DisplayFormat.FormatStr = 'dd mmmmmmmmmm yyyy'
          DisplayFormat.Kind = fkDateTime
          Frame.Typ = []
          Memo.UTF8W = (
            #1055#1088#1072#1081#1089'-'#1083#1080#1089#1090' '#1074#1110#1076' [Date]')
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 102.047310000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset1
        DataSetName = 'frxDBDataset1'
        RowCount = 0
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 192.756027220000000000
          Top = -0.000000119999999999
          Width = 170.078865990000000000
          Height = 18.897651670000000000
          Frame.Typ = []
          Memo.UTF8W = (
            #1058#1086#1088#1075#1110#1074#1077#1083#1100#1085#1072' '#1084#1072#1088#1082#1072)
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710820000000000
          Top = 3.779533260000000000
          Width = 94.488250730000000000
          Height = 18.897644040000000000
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBDataset1."TRADE_MARK"]')
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 83.149660000000000000
        Top = 317.480520000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457041370000000000
          Top = 15.118123990000000000
          Width = 204.094590210000000000
          Height = 18.897644040000000000
          Frame.Typ = []
          Memo.UTF8W = (
            #1057#1090#1086#1088#1110#1085#1082#1072' [Page#] '#1079' [TotalPages#]')
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 64.252010000000000000
        Top = 147.401670000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset3
        DataSetName = 'frxDBDataset3'
        RowCount = 0
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 192.756024690000000000
          Width = 94.488265990000000000
          Height = 18.897644040000000000
          Frame.Typ = []
          Memo.UTF8W = (
            #1058#1086#1074#1072#1088#1085#1072' '#1075#1088#1091#1087#1072)
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 30.236239960000000000
          Top = 3.779530000000000000
          Width = 94.488243100000000000
          Height = 18.897644040000000000
          DataField = 'SERMENT'
          DataSet = frxDBDataset3
          DataSetName = 'frxDBDataset3'
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBDataset3."SERMENT"]')
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574829100000000000
          Top = 41.574831460000000000
          Width = 94.488250730000000000
          Height = 18.897644040000000000
          Frame.Typ = []
          Memo.UTF8W = (
            #1053#1072#1079#1074#1072)
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771804810000000000
          Top = 41.574831460000000000
          Width = 94.488235470000000000
          Height = 18.897644040000000000
          Frame.Typ = []
          Memo.UTF8W = (
            #1062#1110#1085#1072)
        end
      end
      object SubdetailData1: TfrxSubdetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 234.330860000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset2
        DataSetName = 'frxDBDataset2'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795299530000000000
          Top = -0.000002779999999994
          Width = 94.488254550000000000
          Height = 18.897659300000000000
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBDataset2."PROD_ID"]')
          Formats = <
            item
            end
            item
            end>
        end
        object frxDBDataset2PRICE: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'PRICE'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBDataset2."PRICE"]')
        end
      end
    end
  end
  object frxReportTableObject1: TfrxReportTableObject
    Left = 384
    Top = 304
  end
  object frxDBDataset1: TfrxDBDataset
    UserName = 'frxDBDataset1'
    CloseDataSource = False
    DataSource = DataSource1
    BCDToCurrency = False
    DataSetOptions = []
    Left = 600
    Top = 208
  end
  object frxDBDataset2: TfrxDBDataset
    UserName = 'frxDBDataset2'
    CloseDataSource = False
    DataSource = DataSource2
    BCDToCurrency = False
    DataSetOptions = []
    Left = 600
    Top = 264
  end
  object Trade_mark: TFDTable
    Active = True
    IndexFieldNames = 'TRADE_MARK'
    DetailFields = 'TRADE_MARK'
    Connection = UkrserviceConnection
    ResourceOptions.AssignedValues = [rvEscapeExpand]
    TableName = 'TRADE_MARKS'
    Left = 608
    Top = 120
    object Trade_markTRADE_MARK: TStringField
      FieldName = 'TRADE_MARK'
      Origin = 'TRADE_MARK'
      Required = True
      FixedChar = True
      Size = 5
    end
  end
  object frxPDFExport1: TfrxPDFExport
    FileName = 'dddd'
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    EmbedFontsIfProtected = False
    InteractiveForms = True
    InteractiveFormsFontSubset = 'A-Z,a-z,0-9,#43-#47 '
    OpenAfterExport = True
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Title = #1090#1110#1090#1083#1077
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    Creator = 'FastReport'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    PDFStandard = psNone
    PDFVersion = pv17
    Left = 688
    Top = 280
  end
  object frxODTExport1: TfrxODTExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    CreationTime = 0.000000000000000000
    DataOnly = False
    PictureType = gpPNG
    OpenAfterExport = False
    Background = True
    Creator = 'FastReport'
    Language = 'en'
    SuppressPageHeadersFooters = False
    Left = 688
    Top = 336
  end
  object TM_SEG: TFDQuery
    Active = True
    IndexFieldNames = 'TRADE_MARK'
    MasterSource = DataSource1
    MasterFields = 'TRADE_MARK'
    DetailFields = 'SERMENT;TRADE_MARK'
    Connection = UkrserviceConnection
    SQL.Strings = (
      'select distinct trade_marks.trade_mark, products.serment,'
      
        'trim(trade_marks.trade_mark) || products.serment as keyfield fro' +
        'm  trade_marks'
      
        'inner join  products on trade_marks.trade_mark =   products.trad' +
        'e_mark'
      'where products.serment is not null ')
    Left = 528
    Top = 120
  end
  object DataSource3: TDataSource
    DataSet = TM_SEG
    Left = 520
    Top = 32
  end
  object frxDBDataset3: TfrxDBDataset
    UserName = 'frxDBDataset3'
    CloseDataSource = False
    DataSource = DataSource3
    BCDToCurrency = False
    DataSetOptions = []
    Left = 600
    Top = 320
  end
  object productsView: TFDQuery
    Active = True
    DetailFields = 'IS_SELECTED;PRICE;PROD_ID;SERMENT;TRADE_MARK'
    Connection = UkrserviceConnection
    SQL.Strings = (
      'select * from products ')
    Left = 728
    Top = 48
  end
  object DataSource4: TDataSource
    DataSet = productsView
    Left = 720
  end
end
