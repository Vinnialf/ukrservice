object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 442
  ClientWidth = 628
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  TextHeight = 15
  object fqbGrid1: TfqbGrid
    Left = 8
    Top = 8
    Width = 300
    Height = 150
    Columns = <
      item
        Caption = 'Collumn'
        Width = 49
      end
      item
        Caption = 'Visible'
        Width = 49
      end
      item
        Caption = 'Where'
        Width = 49
      end
      item
        Caption = 'Sort'
        Width = 49
      end
      item
        Caption = 'Function'
        Width = 49
      end
      item
        Caption = 'Group'
        Width = 51
      end>
    ColumnClick = False
    DragMode = dmAutomatic
    HideSelection = False
    TabOrder = 0
    ViewStyle = vsReport
  end
  object fqbTableArea1: TfqbTableArea
    Left = 328
    Top = 296
    Width = 185
    Height = 41
    Color = clBtnFace
    ParentColor = False
    TabOrder = 1
  end
  object fqbTableListBox1: TfqbTableListBox
    Left = 176
    Top = 312
    Width = 121
    Height = 97
    Style = lbOwnerDrawFixed
    DragMode = dmAutomatic
    TabOrder = 2
  end
  object fqbDBXEngine1: TfqbDBXEngine
    Left = 512
    Top = 240
  end
  object fqbIBXEngine1: TfqbIBXEngine
    Left = 488
    Top = 120
  end
  object fqbDialog1: TfqbDialog
    Left = 456
    Top = 24
  end
end
