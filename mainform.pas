unit mainform;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, Vcl.DBCGrids, Vcl.Grids,
  Vcl.DBGrids, FireDAC.Comp.Client, FireDAC.Comp.DataSet, frxDesgn, frxClass,
  frCoreClasses, frxTableObject, frxDBSet, frxExportBaseDialog, frxExportPDF,
  Vcl.StdCtrls, Vcl.Buttons, frxExportODF, Vcl.DBCtrls;

type
  TForm1 = class(TForm)
    DataSource1: TDataSource;
    UkrserviceConnection: TFDConnection;
    DataSource2: TDataSource;
    DBGrid2: TDBGrid;
    ProductsReport: TFDQuery;
    frxReport1: TfrxReport;
    frxReportTableObject1: TfrxReportTableObject;
    frxDBDataset1: TfrxDBDataset;
    frxDBDataset2: TfrxDBDataset;
    Trade_mark: TFDTable;
    Trade_markTRADE_MARK: TStringField;
    frxPDFExport1: TfrxPDFExport;
    Button2: TButton;
    frxODTExport1: TfrxODTExport;
    button: TButton;
    TM_SEG: TFDQuery;
    DataSource3: TDataSource;
    frxDBDataset3: TfrxDBDataset;
    ProductsReportPROD_ID: TStringField;
    ProductsReportPRICE: TCurrencyField;
    ProductsReportFK: TStringField;
    productsView: TFDQuery;
    DataSource4: TDataSource;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    DBGrid1: TDBGrid;
    Button1: TButton;
    Button3: TButton;


    procedure buttonClick(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);

   private
    { Private declarations }
  public

  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.Button1Click(Sender: TObject);
       var i: integer;
            j: integer;
begin
                productsView.Active:= false;
                    productsView.SQL.Clear;
                    productsView.SQL.Add('Select * from products where  ');
with DBGrid1.DataSource.DataSet do
      for i:=0 to DBGrid1.SelectedRows.Count-1 do
      begin
        GotoBookmark((DBGrid1.SelectedRows.Items[i]));
        productsView.SQL.Add(' (trade_mark = '+  QuotedStr(FieldByName('Trade_mark').asString) +' ) '  );
        if i<>DBGrid1.SelectedRows.Count-1 then  productsView.SQL.Add(' or ');


         end;

          productsView.Active:= true;
          Button1.Visible:=false;
          DBGrid1.Visible:=false;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
         frxReport1.ShowReport(True);
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
Button1.Visible:=true;
DBGrid1.Visible:=true;
end;

procedure TForm1.buttonClick(Sender: TObject);
begin

       frxReport1.PrepareReport();
       frxReport1.Export(frxODTExport1);
end;

procedure TForm1.CheckBox1Click(Sender: TObject);
begin
    productsView.Active:= false;
      productsView.SQL.Clear;

productsView.SQL.Add('Select * from products');
productsView.SQL.Add('where price>=' + (Edit1.Text) + 'and price<= '  + (Edit2.Text) );
 if CheckBox1.Checked or CheckBox2.Checked then
 begin
  productsView.SQL.Add(' order by ');
  if  CheckBox1.Checked then   productsView.SQL.Add('prod_id');
  if  CheckBox1.Checked and CheckBox2.Checked then   productsView.SQL.Add(' , ');
  if  CheckBox2.Checked then   productsView.SQL.Add('trade_mark');
 end;
                 productsView.Active:= true;
end;

procedure TForm1.CheckBox2Click(Sender: TObject);
begin
Self.CheckBox1Click(self);
end;

procedure TForm1.Edit1Change(Sender: TObject);
begin
     if edit1.Text='' then   edit1.Text:='0';

     Self.CheckBox1Click(self);
end;

procedure TForm1.Edit2Change(Sender: TObject);
begin
 if edit2.Text='' then   edit2.Text:='0';
     Self.CheckBox1Click(self);
end;

end.


